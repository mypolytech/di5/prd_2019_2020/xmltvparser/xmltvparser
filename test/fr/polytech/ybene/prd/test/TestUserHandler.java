package fr.polytech.ybene.prd.test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import fr.polytech.ybene.prd.xmltv.TimeInterval;
import fr.polytech.ybene.prd.xmltv.UserHandler;

class TestUserHandler {

	// === Attributes ===
	private static final Logger LOGGER = LogManager.getLogger(TestUserHandler.class);
	private static SAXParserFactory factory = SAXParserFactory.newInstance();
	private static UserHandler userHandler = new UserHandler();
	private static List<TimeInterval> timeIntervals = new ArrayList<>();
	// ==================

	// === Setup ===
	@BeforeAll
	static void setup() {
		try {
			// Creating parser
			SAXParser saxParser = factory.newSAXParser();

			// Get the parser
			saxParser.parse("./resources/test/test_xmltv.xml", userHandler);

			timeIntervals = userHandler.getTimeIntervals();
		} catch (IOException | ParserConfigurationException | SAXException e) {
			LOGGER.error(e.getMessage());
		}
	}
	// =============

	// === Tests ===
	@Test
	void testTimeIntervalNotEmpty() {
		assertEquals(false, timeIntervals.isEmpty());
	}
	// =============
}

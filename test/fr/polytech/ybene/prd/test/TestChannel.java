package fr.polytech.ybene.prd.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.polytech.ybene.prd.xmltv.Channel;

class TestChannel {

	// === Attributes ===
	private static String channelCode;
	// ==================
	
	// === Setup ===
	@BeforeAll
	static void setup() {
		channelCode = "C111.api.telerama.fr";
	}
	// =============
	
	// === Test ===
	@Test
	void testChannelNameFromCode() {
		assertEquals("ARTE", Channel.getChannelFromCode(channelCode).getChannelName());
	}
	
	@Test
	void testChannelNameFromWrongCode() {
		assertEquals(null, Channel.getChannelFromCode(""));
	}
	// ============
}

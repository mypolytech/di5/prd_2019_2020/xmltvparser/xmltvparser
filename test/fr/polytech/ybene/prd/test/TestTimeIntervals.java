package fr.polytech.ybene.prd.test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import fr.polytech.ybene.prd.xmltv.TimeInterval;
import fr.polytech.ybene.prd.xmltv.UserHandler;

class TestTimeIntervals {
	
	// === Attributes ===
	private static final Logger LOGGER = LogManager.getLogger(TestTimeIntervals.class);
	private static SAXParserFactory factory = SAXParserFactory.newInstance();
	private static UserHandler userHandler = new UserHandler();
	private static List<TimeInterval> timeIntervals = new ArrayList<>();
	// ==================
	
	// === Setup ===
	@BeforeAll
	static void setup() {		
		try {
			// Creating parser
			SAXParser saxParser = factory.newSAXParser();
			
			// Get the parser
			saxParser.parse("./resources/test/test_xmltv.xml", userHandler);
			
			timeIntervals = userHandler.getTimeIntervals();
		} catch (IOException | ParserConfigurationException | SAXException e) {
			LOGGER.error(e.getMessage());
		}
	}
	// =============

	@Test
	void testTimeIntervalName() {
		assertEquals("ARTE", timeIntervals.get(0).getChannel().getChannelName());
	}
	
	@Test
	void testTimeIntervalStartingDate() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssZ");
		
		try {
			Date date = simpleDateFormat.parse("20200212063500 +0100");
			calendar.setTime(date);
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());
		}
		
		assertEquals(calendar, timeIntervals.get(0).getStartingDate());
	}
	
	@Test
	void testTimeIntervalEndingDate() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssZ");
		
		try {
			Date date = simpleDateFormat.parse("20200212070500 +0100");
			calendar.setTime(date);
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());
		}
		
		
		assertEquals(calendar, timeIntervals.get(0).getEndingDate());
	}
}

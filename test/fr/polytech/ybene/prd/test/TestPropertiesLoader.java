package fr.polytech.ybene.prd.test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.io.InputStreamReader;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.polytech.ybene.prd.tools.PropertiesLoader;

class TestPropertiesLoader {
	
	// === Attributes ===
	private static PropertiesLoader propertiesLoader;
	private static PropertiesLoader propLoader = new PropertiesLoader("");
	// ==================
	
	// === Setup ===
	@BeforeAll
	static void setup() {
		propertiesLoader = new PropertiesLoader("./resources/test/test.properties");
	}
	// =============
	
	// === Tests ===
	@Test
	void testLoadFileName() {
		assertEquals("name", propertiesLoader.getFileName());
	}
	
	@Test
	void testLoadFilePath() {
		assertEquals("path", propertiesLoader.getFilePath());
	}
	
	@Test
	void testLoadCSV() {
		assertEquals("csv", propertiesLoader.getCSVFile());
	}
	
	@Test
	void testLoadNotExistingProperty() {
		String test = propertiesLoader.getProperties().getProperty("NOT-EXISTING");
		assertEquals(null, test);
	}
	
	@Test
	void testNotExistingFile() {
		assertThrows(FileNotFoundException.class, () -> {
			InputStreamReader inputStreamReader = PropertiesLoader.openPropertiesFile("");
			inputStreamReader.close();
		});
	}
	
	@Test
	void testNullProperties() {
		assertEquals(null, propLoader.getProperties());
	}
	
	@Test
	void testNullFilePath() {
		assertEquals("", propLoader.getFilePath());
	}
	
	@Test
	void testNullFileName() {
		assertEquals("", propLoader.getFileName());
	}
	
	@Test
	void testNullCSVFile() {
		assertEquals("", propLoader.getCSVFile());
	}
	// =============
}

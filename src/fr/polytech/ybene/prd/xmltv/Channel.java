package fr.polytech.ybene.prd.xmltv;

/**
 * Channel represents all possible channels used in the project.
 * There are two values which define the channel :
 * - The channel code in the XMLTV file
 * - The channel name
 * @author Yohann BENETREAULT
 * @version 1.0
 */

public enum Channel {
	ARTE("C111.api.telerama.fr", "ARTE"),
	LCI("C112.api.telerama.fr", "LCI"),
	M6("C118.api.telerama.fr", "M6"),
	W9("C119.api.telerama.fr", "W9"),
	CHERIE25("C1399.api.telerama.fr", "CHERIE25"),
	RMC_DECOUVERTE("C1400.api.telerama.fr", "RMC DECOUVERTE"),
	EQUIPE("C1401.api.telerama.fr", "L_EQUIPE"),
	RMC_STORY("C1402.api.telerama.fr", "RMC STORY"),
	TER("C1403.api.telerama.fr", "6TER"),
	TF1_S_F("C1404.api.telerama.fr", "TF1 SERIES FILMS"),
	FRANCEO("C160.api.telerama.fr", "FRANCE_O"),
	TF1("C192.api.telerama.fr", "TF1"),
	FRANCE3("C1928.api.telerama.fr", "FRANCE_3"),
	TMC("C195.api.telerama.fr", "TMC"),
	FRANCEINFO("C2111.api.telerama.fr", "FRANCEINFO"),
	CNEWS("C226.api.telerama.fr", "CNEWS"),
	LCP("C234.api.telerama.fr", "LA_CHAINE_PARLEMENTAIRE"),
	FRANCE2("C4.api.telerama.fr", "FRANCE_2"),
	NRJ12("C444.api.telerama.fr", "NRJ_12"),
	C8("C445.api.telerama.fr", "C8"),
	TFX("C446.api.telerama.fr", "TFX"),
	CSTAR("C458.api.telerama.fr", "CSTAR"),
	FRANCE5("C47.api.telerama.fr", "FRANCE_5"),
	BFMTV("C481.api.telerama.fr", "BFMTV"),
	GULLI("C482.api.telerama.fr", "GULLI"),
	FRANCE4("C78.api.telerama.fr", "FRANCE_4");
	
	// === Attributes ===
	private String channelCode = "";
	private String channelName = "";
	// ==================
		
	// === Constructor ===
	Channel(String code, String name) {
		this.channelCode = code;
		this.channelName = name;
	}
	// ===================
	
	// === Getters ===
	public String getChannelCode() {
		return channelCode;
	}
	
	public String getChannelName() {
		return channelName;
	}
	// ===============

	// === Methods ===
	/**
	 * Get the channel name with its code
	 * @param code the code in the XMLTV file
	 * @return
	 */
	public static Channel getChannelFromCode(String code) {
		// Get the channel with the code
		for(Channel chan: Channel.values()) {
			if(chan.getChannelCode().equals(code)) {
				return chan;
			}
		}
		
		// The code doesn't exist
		return null;
	}
	
	@Override
	public String toString() {
		return channelCode + " " + channelName;
	}
	// ===============
}



package fr.polytech.ybene.prd.xmltv;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * TimeInterval represents a time interval in which we have to record a video from the TV Station.
 * It contains all necessary informations such as the channel, the beginning date and the ending date.
 * @author Yohann BENETREAULT
 * @version 1.0
 */

public class TimeInterval implements Comparable<TimeInterval> {
	
	// === Attributes ===
	static final Logger LOGGER = LogManager.getLogger(TimeInterval.class);
	private Calendar startingDate;
	private Calendar endingDate;
	private Channel channel;
	private String rawStart;
	private String rawStop;
	private long length;
	// ==================

	// === Constructor ===
	/**
	 * Constructs a TimeInterval with all necessary informations
	 * @param channelCode the channel code
	 * @param start the start date
	 * @param stop the end date
	 */
	public TimeInterval(String channelCode, String start, String stop) {
		this.rawStart = start;
		this.rawStop = stop;
		formatRawTimes();
		
		this.length = this.endingDate.getTimeInMillis() - this.startingDate.getTimeInMillis();
		
		this.channel = Channel.getChannelFromCode(channelCode);
	}
	// ===================
	
	// === Getters ===
	public Calendar getStartingDate() {
		return this.startingDate;
	}
	public Calendar getEndingDate() {
		return this.endingDate;
	}
	
	public Channel getChannel() {
		return this.channel;
	}

	public String getStart() {
		return this.rawStart;
	}
	
	public String getStop() {
		return this.rawStop;
	}
	// ===============
	
	@Override
	public String toString() {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append("Channel :");
		strBuilder.append(this.channel.toString());
		strBuilder.append("\n");
		strBuilder.append("Start :");
		strBuilder.append(this.rawStart);
		strBuilder.append("\n");
		strBuilder.append("Stop :");
		strBuilder.append(this.rawStop);
		strBuilder.append("\n");
		strBuilder.append("\n");
		
		return strBuilder.toString();
	}
	
	/**
	 * Transforms the TimeInterval into a CSV line
	 * @return the CSV line
	 */
	public String formatedCSVString() {
		StringBuilder stringBuilder = new StringBuilder();
		
		// Channel
		stringBuilder.append(this.channel.getChannelName());
		stringBuilder.append(";");
		// Starting time
		stringBuilder.append(this.startingDate.getTimeInMillis());
		stringBuilder.append(";");
		// Ending time
		stringBuilder.append(this.endingDate.getTimeInMillis());
		stringBuilder.append(";");
		// Program length
		stringBuilder.append(this.length);
		stringBuilder.append("\n");
		
		return stringBuilder.toString();
	}
	
	/**
	 * This method converts the string into two Date objects (start and end). 
	 * The XMLTV time format is "AAAAMMDDHHMMSS +TimeZone".
	 */
	private void formatRawTimes() {
		// Creating format
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssZ");
		
		// Creating calendars
		startingDate = Calendar.getInstance();
		endingDate = Calendar.getInstance();

		try {
			// Taking care of the starting date
			Date rawStartingDate = simpleDateFormat.parse(rawStart);
			startingDate.setTime(rawStartingDate);
		} catch (ParseException parseException) {
			startingDate = null;			
		}
			
		try {
			// Taking care of the ending date
			Date rawEndingDate = simpleDateFormat.parse(rawStop);
			endingDate.setTime(rawEndingDate);
		} catch (ParseException parseException) {
			endingDate = null;
		}
	}
	
	@Override
	public int compareTo(TimeInterval tInter) {
		
		if(channel.equals(tInter.getChannel())) {			
			return this.startingDate.compareTo(tInter.getStartingDate());
		} else {
			return this.channel.compareTo(tInter.getChannel());
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
}

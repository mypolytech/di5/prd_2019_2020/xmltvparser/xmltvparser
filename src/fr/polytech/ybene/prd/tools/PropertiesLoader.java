package fr.polytech.ybene.prd.tools;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * PropertiesLoader loads the project properties from a .properties file.
 * Properties are :
 * - XML File name (FILE_NAME=value)
 * - XML File path (FILE_PATH=value)
 * - CSV File (CSV_FILE=value)
 * @author Yohann BENETREAULT
 * @version 1.0
 */

public class PropertiesLoader {

	// === Attributes ===
	private static final Logger LOGGER = LogManager.getLogger(PropertiesLoader.class);
	private Properties properties;
	private String fileName = "";
	private String filePath = "";
	private String csvFile = "";
	// ==================
	
	// === Constructor ===
	/**
	 * Constructs a PropertiesLoader from a properties file.
	 * @param propertiesFilePath the .properties file path
	 */
	public PropertiesLoader(String propertiesFilePath) {
		this.loadProperties(propertiesFilePath);
		
		if (this.properties != null) {			
			this.fileName = properties.getProperty("FILE_NAME");
			this.filePath = properties.getProperty("FILE_PATH");
			this.csvFile = properties.getProperty("CSV_FILE");
		}
		
	}
	// ===================
	
	// === Getters ===
	public Properties getProperties() {
		return properties;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFilePath() {
		return filePath;
	}
	
	public String getCSVFile() {
		return csvFile;
	}
	// ===============

	// === Methods ===
	/**
	 * Load properties from a .properties file.
	 * @param propertiesFilePath the path to the .properties file
	 */
	private void loadProperties(String propertiesFilePath) {
		// Try-with-resources
		try (InputStreamReader inputStreamReader = openPropertiesFile(propertiesFilePath)) {
			this.properties = new Properties();
			this.properties.load(inputStreamReader);
		} catch (FileNotFoundException fileNotFoundException) {
			LOGGER.error(fileNotFoundException.getMessage());
		} catch (IOException ioException) {
			LOGGER.error(ioException.getMessage());
		}
	}

	/**
	 * Open the properties file and return an InputStreamReader
	 * @param propertiesFilePath the properties file path
	 * @return An InputStreamReader of the file
	 * @throws FileNotFoundException
	 */
	public static InputStreamReader openPropertiesFile(String propertiesFilePath) throws FileNotFoundException {
		return new InputStreamReader(new FileInputStream(propertiesFilePath));
	}
	// ===============
}

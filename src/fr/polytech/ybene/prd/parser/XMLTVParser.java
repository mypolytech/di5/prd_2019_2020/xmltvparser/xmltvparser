package fr.polytech.ybene.prd.parser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import fr.polytech.ybene.prd.tools.PropertiesLoader;
import fr.polytech.ybene.prd.xmltv.TimeInterval;
import fr.polytech.ybene.prd.xmltv.UserHandler;

public class XMLTVParser {

	private static final Logger LOGGER = LogManager.getLogger(XMLTVParser.class);

	public static void main(String[] argv) {

		BasicConfigurator.configure();


		try {
			
			String filePath = "";
			String fileName = "";
			String csvFile = "";
			
			if(argv.length > 0) {
				// Get parameters
				String param = argv[0];
				// Split string
				String[] params = param.split("/");
				
				// Parse the string
				StringBuilder stringBuilder = new StringBuilder();
				int paramsLength = params.length;
				
				for(int i = 0; i < paramsLength - 1; ++i) {
					stringBuilder.append(params[i]);
					stringBuilder.append("/");
				}

				filePath = stringBuilder.toString();
				fileName = params[paramsLength - 1];
				
				if(argv.length >= 2) {
					csvFile = argv[1];
				} else {				
					String currentPath = new File(".").getCanonicalPath();
					csvFile = currentPath + "/time_intervals.csv";
				}
			} else {
				// Loading properties
				LOGGER.info("Loading properties...");
				PropertiesLoader propertiesLoader = new PropertiesLoader("./resources/parser.properties");
				filePath = propertiesLoader.getFilePath();
				fileName = propertiesLoader.getFileName();
				csvFile = propertiesLoader.getCSVFile();
			}
			
			// Creating SAX parser
			LOGGER.info("Creating SAX parser...");
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			// Creating the handler
			LOGGER.info("Creating UserHandler...");
			UserHandler userHandler = new UserHandler();

			LOGGER.info("== START : PARSING ==");
			// Parsing XML file
			LOGGER.info("Processing XML file parsing...");
			saxParser.parse(filePath + fileName, userHandler);
			LOGGER.info("== END : PARSING ==");

			// Creating TimeInterval objects
			List<TimeInterval> timeIntervals = userHandler.getTimeIntervals();
			Collections.sort(timeIntervals);

			// Getting statistics
			Integer size = timeIntervals.size();

			// Printing statistics
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(size.toString());
			stringBuilder.append(" programms parsed.");
			LOGGER.info(stringBuilder.toString());

			String outputFilePath = csvFile;
			writeTimeIntervals(timeIntervals, outputFilePath);
		} catch (Exception exception) {
			LOGGER.error(exception.getMessage());
		}
	}

	/**
	 * Write a TimeInterval list in a CSV file.
	 * 
	 * @param timeIntervals
	 * @param outputFilePath
	 * @throws IOException
	 */
	public static void writeTimeIntervals(List<TimeInterval> timeIntervals, String outputFilePath) throws IOException {
		File outputFile = new File(outputFilePath);
		File file = new File("./resources/backup.csv");

		// CSV with TV programs
		try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
			for (TimeInterval timeInterval : timeIntervals) {
				fileOutputStream.write(timeInterval.formatedCSVString().getBytes());
			}
		}

		// CSV with AD breaks
		try (FileOutputStream fileOutputStream = new FileOutputStream(outputFile)) {

			int arraySize = timeIntervals.size();

			for (int i = 0; i < arraySize; ++i) {
				// Constructs the string to be written in CSV file
				StringBuilder stringBuilder = new StringBuilder();

				// We compare two elements, so we need to be sure that there is a next element
				if ((i + 1) < arraySize) {
					String channelName = timeIntervals.get(i).getChannel().getChannelName();
					String nextChannelName = timeIntervals.get(i + 1).getChannel().getChannelName();
					
					// The program ends at the same time as the next one starts, so we need to
					// introduce a delta so we can capture AD breaks
					int delta = 5 * 60 * 1000;

					if (channelName.equals(nextChannelName)) {
						stringBuilder.append(channelName);
						stringBuilder.append(";");
						stringBuilder.append(timeIntervals.get(i).getEndingDate().getTimeInMillis() - delta);
						stringBuilder.append(";");
						stringBuilder.append(timeIntervals.get(i).getStartingDate().getTimeInMillis() + delta);
						stringBuilder.append("\n");
					}

					fileOutputStream.write(stringBuilder.toString().getBytes());
				}

			}
		}
	}
}

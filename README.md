# Parser XMLTV

Le but de ce parser est d'extraire des intervals de temps dans un fichier CSV depuis un fichier XMLTV.

XMLTV est un fichier au format XML et qui est composé de tous les programmes TVs sur quelques jours. Pour chaque programme de nombreuses informations sont disponibles, dans notre cas celles qui nous intéressent sont :

* La chaine TV
* La date de début
* La date de fin